import { request } from 'umi';

export async function fakeSubmitForm(params: any) {
  return request('/api/stepForm', {
    method: 'POST',
    data: params,
  });
}

export async function objectDetail(projectCode: string, objectCode: string) {
  return request(`/api/dev/object/details/${projectCode}/${objectCode}`, {
    method: 'GET',
  });
}
