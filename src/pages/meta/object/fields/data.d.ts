export interface MetaObject {
  objectId?: string;
  tenantId?: string;
  projectCode: string;
  objectCode: string;
  objectName: string;
  objectType: string;
  fields?: [];
}

export type CurrentTypes = 'base' | 'confirm' | 'result';
