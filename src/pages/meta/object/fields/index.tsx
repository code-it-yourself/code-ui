import React, { useRef, useState } from 'react';
import type { FormInstance } from 'antd';
import { Card, Result, Button, Divider } from 'antd';
import { PageContainer } from '@ant-design/pro-layout';
import {
  ModalForm, ProForm, StepsForm,
  ProFormDigit, ProFormSelect, ProFormText, ProFormDatePicker, ProFormDependency
} from '@ant-design/pro-form';
import { Form } from 'antd';
import type { MetaObject } from './data.d';
import styles from './style.less';
import {EditableProTable, ProColumnType} from "@ant-design/pro-table";
import {useLocation} from "react-router";
import {objectDetail} from "@/pages/meta/object/fields/service";

type MetaField = {
  fieldId: React.Key;
  tenantId: string;
  projectCode: string;
  objectCode: string;
  fieldCode: string;
  fieldName?: string;
  defaultValue?: string;
  dataType: string;
  dateFormat?: string;
  example?: string;
  maxLength?: number;
  numericScale?: number;
  dataFieldPos: number;
  relateToOne?: string;
  relateToMany?: number;
  requireFlag?: boolean;
  uniqueFlag?: boolean;
  indexFlag?: boolean;
  systemFlag?: boolean;
  sortNo?: number;
  startValue?: number;
  maxSize?: number;
  relateObject?: string;
  relateField?: string;
};

const StepResult: React.FC<{
  onFinish: () => Promise<void>;
}> = (props) => {
  return (
    <Result
      status="success"
      title="保存成功"
      subTitle="对象模型已创建，可以开始操作对象了！"
      extra={
        <>
          <Button type="primary" onClick={props.onFinish}>
            新增对象
          </Button>
          <Button>查看对象</Button>
        </>
      }
      className={styles.result}
    >
      {props.children}
    </Result>
  );
};



const StepForm: React.FC<Record<string, any>> = () => {
  const location: any = useLocation();
  const projectCode = location.query.projectCode;
  const objectCode = location.query.objectCode;
  const [stepData, setStepData] = useState<MetaObject>({
    objectType: 'Business',
    projectCode: projectCode,
    objectCode: objectCode,
    objectName: '',
    fields: [],
  });

  const formRef = useRef<FormInstance>();
  const [current, setCurrent] = useState(location.query.step * 1 || 0);
  const [form] = Form.useForm<MetaField>();

  objectDetail(projectCode, objectCode).then(response => {
    formRef.current?.setFieldsValue(response.data);
  });

  const [dataSource, setDataSource] = useState<MetaField[]>([]);

  const columns: ProColumnType<MetaField>[] = [
    {
      title: '属性编码',
      dataIndex: 'fieldCode',
      key: 'fieldCode',
      width: '15%',
    },
    {
      title: '属性名称',
      dataIndex: 'fieldName',
      key: 'fieldName',
      width: '15%',
    },
    {
      title: '数据类型',
      dataIndex: 'dataType',
      key: 'dataType',
      width: '10%',
      valueType: 'select',
      // 自动编号、文本、枚举、级联枚举、实数、日期时间、整数、图片、文件、地理位置、关联关系、多选关联、主子关系、计算公式、富文本
      valueEnum: {
        'id': '自动编号',
        'text': '文本',
        'date': '日期时间',
        'integer': '整数',
        'decimal': '实数',
        'encryption': '加密文本',
        'image': '图片',
        'file': '文件',
        'toOne': '一对一',
        'toMany': '一对多',
        'manyToMany': '多对多',
      },
    },
    {
      title: '默认值',
      dataIndex: 'defaultValue',
      key: 'defaultValue',
      width: '10%',
    },
    {
      title: '必填',
      dataIndex: 'requireFlag',
      key: 'requireFlag',
      width: '8%',
      valueType: 'select',
      valueEnum: {
        '1': '是',
        '0': '否',
      },
    },
    {
      title: '唯一',
      dataIndex: 'uniqueFlag',
      key: 'uniqueFlag',
      width: '8%',
      valueType: 'select',
      valueEnum: {
        '1': '是',
        '0': '否',
      },
    },
    {
      title: '索引',
      dataIndex: 'indexFlag',
      key: 'indexFlag',
      width: '8%',
      valueType: 'select',
      valueEnum: {
        '1': '是',
        '0': '否',
      },
    },
    {
      title: '别名',
      dataIndex: 'aliases',
      key: 'aliases',
      width: '10%',
    },
    {
      title: '操作',
      key: 'action',
      valueType: 'option',
      render: (_, record: MetaField, index, action) => {
        return [
          <a
            key="eidit"
            onClick={() => {
              action?.startEditable(record.fieldId);
            }}
          >
            编辑
          </a>,

          <ModalForm<MetaField>
              title="高级设置"
              trigger={
                <a key="setting">高级设置</a>
              }
              form={form}
              autoFocusFirstInput
              modalProps={{
                destroyOnClose: true,
                onCancel: () => console.log('run'),
              }}
              submitTimeout={2000}
              onFinish={async (values) => {
                console.log(values);
                return true;
              }}
              initialValues={record}
          >
            {['encryption', 'text'].includes(record.dataType) ? (
                <ProForm.Group label="设置文本">
                  <ProFormDigit label="字符串长度" name="maxLength" width="sm" min={1} max={255} />
                  <ProFormText label="默认值" name="default-value" placeholder="请输入" width="md" />
                </ProForm.Group>
            ) : null}

            {record.dataType === 'integer' ? (
                <ProForm.Group label="设置整数">
                  <ProFormDigit label="整数长度" name="maxLength" width="sm" min={1} max={20} addonAfter="位" />
                  <ProFormDigit label="默认值" name="default-value" width="sm" />
                </ProForm.Group>
            ) : null}
            {record.dataType === 'decimal' ? (
                <ProForm.Group label="设置实数">
                  <ProFormDigit label="整数长度" name="maxLength" width="sm" min={1} max={20} addonAfter="位" />
                  <ProFormDigit label="保留小数" name="numericScale" width="sm" min={1} max={10} addonAfter="位" />
                  <ProFormDigit label="默认值" name="default-value" width="sm" />
                </ProForm.Group>
            ) : null}
            {record.dataType === 'date' ? (
                <ProForm.Group label="设置时间">
                  <ProFormSelect
                      width="sm"
                      options={[
                        {
                          value: 'yyyy-MM-dd HH:mm:ss',
                          label: '年月日+时分秒',
                        },
                        {
                          value: 'yyyy-MM-dd',
                          label: '年月日',
                        },
                        {
                          value: 'yyyy-MM',
                          label: '年月',
                        },
                      ]}
                      initialValue='yyyy-MM-dd HH:mm:ss'
                      name="dateFormat"
                      label="格式"
                      fieldProps={{
                        onChange(value: string) {
                          switch (value) {
                            case 'yyyy-MM':
                              record.example = '2022-12';
                              break;
                            case 'yyyy-MM-dd':
                              record.example = '2022-12-03';
                              break;
                            case 'yyyy-MM-dd HH:mm:ss':
                            default:
                              record.example = '2022-12-03 15:08:07';
                              break;
                          }
                          form.setFieldsValue(record);
                        }
                      }}
                  />
                  <ProFormDatePicker label="默认值" name="default-value" placeholder="请选择" width="md" />
                  <ProFormText name="example" readonly={true} label="示例" initialValue="2022-12-03 15:08:07" />
                </ProForm.Group>
            ) : null}
            {record.dataType === 'id' ? (
                <ProForm.Group label="设置自动编号">
                  <ProFormDigit label="起始值" name="startValue" width="sm" min={1} />
                </ProForm.Group>
            ) : null}
            {record.dataType === 'image' ? (
                <ProForm.Group label="设置图片">
                  <ProFormDigit label="图片大小上限" name="maxSize" width="sm" min={1} max={10} addonAfter="M" />
                </ProForm.Group>
            ) : null}
            {record.dataType === 'file' ? (
                <ProForm.Group label="设置文件">
                  <ProFormDigit label="文件大小上限" name="maxSize" width="sm" min={1} max={10} addonAfter="M" />
                </ProForm.Group>
            ) : null}
            {['toOne', 'toMany', 'manyToMany'].includes(record.dataType)  ? (
                <ProForm.Group label="设置关联关系">
                  <ProFormSelect
                      request={async () => [
                        {
                          value: 'SysConfig',
                          label: '系统配置',
                        },
                        {
                          value: 'SysUser',
                          label: '系统用户',
                        },
                        {
                          value: 'SysRole',
                          label: '系统角色',
                        },
                        {
                          value: 'AuthInfo',
                          label: '认证信息',
                        },
                      ]}
                      width="sm"
                      name="relateObject"
                      label="绑定对象"
                      fieldProps={{
                        onChange(value: string) {
                          record.relateObject = value;
                          record.relateField = '';
                          form.setFieldsValue(record);
                        }
                      }}
                  />
                  {/*  ProFormDependency 会自动注入并且 进行 shouldUpdate 的比对  */}
                  <ProFormDependency name={['relateObject']}>
                    {({ relateObject }) => {
                      return (
                          <ProFormSelect
                              params={relateObject}
                              request={async () => {
                                const options = [];
                                switch (relateObject) {
                                  case 'SysConfig':
                                    options.push({
                                      value: 'id',
                                      label: '全局配置id',
                                    });
                                    options.push({
                                      value: 'key',
                                      label: '配置键',
                                    });
                                    options.push({
                                      value: 'value',
                                      label: '配置值',
                                    });
                                    break;
                                  default:
                                    options.push({
                                      value: 'userId',
                                      label: '用户Id',
                                    });
                                    options.push({
                                      value: 'userCode',
                                      label: '用户编码',
                                    });
                                    options.push({
                                      value: 'userName',
                                      label: '用户名',
                                    });
                                    break;
                                }
                                return options;
                              }}
                              width="md"
                              name="relateField"
                              label={`请选择${relateObject || ''}对象下的属性`}
                          />
                      );
                    }}
                  </ProFormDependency>
                </ProForm.Group>
            ) : null}

            <ProFormText name="remarks" label="备注" placeholder="请输入" />
          </ModalForm>,
        ];
      },
    },
  ];

  return (
    <PageContainer content="对象模型指属性和操作方法封装在对象类的结构中，可以通过将一个对象类嵌套或封装在另一个类里来表示类间的关联。">
      <Card bordered={false}>
        <StepsForm
          current={current}
          onCurrentChange={setCurrent}
          submitter={{
            render: (props, dom) => {
              if (props.step === 2) {
                return null;
              }
              return dom;
            },
          }}
        >
          <StepsForm.StepForm<MetaObject>
            formRef={formRef}
            title="填写对象信息"
            initialValues={stepData}
            onFinish={async (values) => {
              setStepData(values);
              return true;
            }}
          >
            <ProFormSelect
              label="所属项目"
              width="md"
              name="projectCode"
            />
            <ProFormSelect
              label="对象类型"
              width="md"
              name="objectType"
              rules={[{ required: true, message: '请选择对象类型' }]}
              valueEnum={{
                'System': '系统预置对象',
                'Business': '业务对象',
              }}
            />
            <ProFormText
              label="对象编码"
              width="md"
              name="objectCode"
              rules={[{ required: true, message: '请输入对象编码' }]}
              placeholder="请输入对象编码"
            />
            <ProFormText
              label="对象名称"
              name="objectName"
              width="md"
              rules={[
                { required: true, message: '请输入对象名称' },
              ]}
              placeholder="请输入对象名称"
            />
            <ProFormText
              label="说明"
              name="remarks"
              width="md"
            />


          </StepsForm.StepForm>

          <StepsForm.StepForm title="配置对象属性">
            <div>
              <EditableProTable<MetaField>
                rowKey="fieldId"
                recordCreatorProps={
                  {
                    creatorButtonText: '新增字段',
                    record: (index) => ({
                      fieldId: (Math.random() * 1000000).toFixed(0),
                      projectCode: projectCode,
                      objectCode: objectCode,
                      dataType: 'text',
                      dataFieldPos: index,
                    } as MetaField),
                  }
                }
                columns={columns}
                request={async () => {
                  const response = await objectDetail(projectCode, objectCode);
                  return {
                    data: response.data.fields,
                    // success 请返回 true，
                    // 不然 table 会停止解析数据，即使有数据
                    success: response.success,
                    // 不传会使用 data 的长度，如果是分页一定要传
                    total: response.data.fields.length,
                  };
                }}
                value={dataSource}
                onChange={setDataSource}
              />
            </div>
          </StepsForm.StepForm>
          <StepsForm.StepForm title="完成">
            <StepResult
              onFinish={async () => {
                setCurrent(0);
                formRef.current?.resetFields();
              }}
            >
            </StepResult>
          </StepsForm.StepForm>
        </StepsForm>
        <Divider style={{ margin: '40px 0 24px' }} />
        <div className={styles.desc}>
          <h3>说明</h3>
          <ul>
            <li>对象名称用于页面定义、对象模型、权限模板里选择显示，至多100字符，可输入汉字、字母、数字、下划线等内容。</li>
            <li>字段分为系统标准字段和自定义字段两种</li>
            <li>系统标准字段在创建业务对象时自动生成，用于存储标准的业务数据，例如：创建人、时间等。</li>
            <li>自定义字段根据业务需求自行创建，自定义字段可通过对象中的“新增字段”添加。</li>
          </ul>
        </div>
      </Card>
    </PageContainer>
  );
};

export default StepForm;
