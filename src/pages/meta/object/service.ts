import { request } from 'umi';
import type { CriteriaQuery, ObjectPage } from './data.d';

export async function pageObject(
  params: CriteriaQuery,
): Promise<{ data: ObjectPage }> {
  return request('/api/dev/object/page', {
    method: 'POST',
    data: params,
  });
}
