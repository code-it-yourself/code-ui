import {
  DownloadOutlined,
  EditOutlined,
  EllipsisOutlined,
  ShareAltOutlined,
} from '@ant-design/icons';
import { Avatar, Card, Col, Dropdown, Form, List, Menu, Row, Select, Tooltip } from 'antd';
import numeral from 'numeral';
import type { FC } from 'react';
import React from 'react';
import {Link, useRequest} from 'umi';
import StandardFormRow from './components/StandardFormRow';
import TagSelect from './components/TagSelect';
import { pageObject } from './service';
import styles from './style.less';
import {ObjectInstance} from "./data.d";

const { Option } = Select;

export function formatWan(val: number) {
  const v = val * 1;
  if (!v || Number.isNaN(v)) return '0';

  let result: React.ReactNode = val;
  if (val > 10000) {
    result = (
      <span>
        {Math.floor(val / 10000)}
        <span
          style={{
            position: 'relative',
            top: -2,
            fontSize: 14,
            fontStyle: 'normal',
            marginLeft: 2,
          }}
        >
          万
        </span>
      </span>
    );
  }
  return result;
}

const formItemLayout = {
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
};

const CardInfo: React.FC<{
  fieldNum: React.ReactNode;
  methodNum: React.ReactNode;
}> = ({ fieldNum, methodNum }) => (
  <div className={styles.cardInfo}>
    <div>
      <p>属性</p>
      <p>{fieldNum}</p>
    </div>
    <div>
      <p>方法</p>
      <p>{methodNum}</p>
    </div>
  </div>
);

export const Objects: FC<Record<string, any>> = () => {
  const { data, loading, run } = useRequest((values: any) => {
    console.log('form data', values);
    return pageObject({
      criteriaList: [
        {
          column: 'projectCode',
          predicate: 'EQ',
          value: 'SuperAdmin'
        }
      ],
      orderByList: [
        {
          column: 'createAt',
          order: 'DESC'
        }
      ],
      page: {
        pageSize: 20,
        pageIndex: 1
      }
    });
  });

  console.log('form data', data);

  const list = data?.records || [];

  const itemMenu = (
    <Menu>
      <Menu.Item>
        <a target="_blank" rel="noopener noreferrer" href="https://www.alipay.com/">
          接口文档
        </a>
      </Menu.Item>
    </Menu>
  );

  return (
    <div className={styles.filterCardList}>
      <Card bordered={false}>
        <Form
          onValuesChange={(_, values) => {
            run(values);
          }}
        >
          <StandardFormRow title="所属类目" block style={{ paddingBottom: 0 }}>
            <Form.Item name="category">
              <TagSelect>
                <TagSelect.Option value="cat1">系统默认</TagSelect.Option>
                <TagSelect.Option value="cat2">电商</TagSelect.Option>
                <TagSelect.Option value="cat3">股票</TagSelect.Option>
                <TagSelect.Option value="cat4">记账</TagSelect.Option>
                <TagSelect.Option value="cat5">博客</TagSelect.Option>
              </TagSelect>
            </Form.Item>
          </StandardFormRow>
          <StandardFormRow title="其它选项" grid last>
            <Row gutter={16}>
              <Col lg={8} md={10} sm={10} xs={24}>
                <Form.Item {...formItemLayout} name="author" label="作者">
                  <Select placeholder="不限" style={{ maxWidth: 200, width: '100%' }}>
                    <Option value="lisa">王昭君</Option>
                  </Select>
                </Form.Item>
              </Col>
              <Col lg={8} md={10} sm={10} xs={24}>
                <Form.Item {...formItemLayout} name="rate" label="好评度">
                  <Select placeholder="不限" style={{ maxWidth: 200, width: '100%' }}>
                    <Option value="good">优秀</Option>
                    <Option value="normal">普通</Option>
                  </Select>
                </Form.Item>
              </Col>
            </Row>
          </StandardFormRow>
        </Form>
      </Card>
      <br />
      <List<ObjectInstance>
        rowKey="objectCode"
        grid={{
          gutter: 16,
          xs: 1,
          sm: 2,
          md: 3,
          lg: 3,
          xl: 4,
          xxl: 4,
        }}
        loading={loading}
        dataSource={list}
        renderItem={(item) => (
          <List.Item key={item.objectCode}>
            <Card
              hoverable
              bodyStyle={{ paddingBottom: 20 }}
              actions={[
                <Tooltip key="edit" title="编辑">
                  <Link to={`object/fields?step=0&projectCode=${item.projectCode}&objectCode=${item.objectCode}`}><EditOutlined /></Link>
                </Tooltip>,
                <Tooltip key="fields" title="属性维护">
                  <Link to={`object/fields?step=1&projectCode=${item.projectCode}&objectCode=${item.objectCode}`}>
                    <ShareAltOutlined />
                  </Link>
                </Tooltip>,
                <Tooltip key="download" title="导出模型">
                  <DownloadOutlined />
                </Tooltip>,
                <Dropdown key="ellipsis" overlay={itemMenu}>
                  <EllipsisOutlined />
                </Dropdown>,
              ]}
            >
              <Card.Meta avatar={<Avatar size="small" src="https://www.codeiy.com/images/lowcode/class.png" />} title={item.objectName} />
              <div className={styles.cardItemContent}>
                <CardInfo
                  fieldNum={formatWan(item.fieldNum)}
                  methodNum={numeral(item.methodNum).format('0,0')}
                />
              </div>
            </Card>
          </List.Item>
        )}
      />
    </div>
  );
};

export default Objects;
