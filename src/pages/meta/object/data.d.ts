export type Member = {
  avatar: string;
  name: string;
  id: string;
};

export interface CriteriaQuery {
  criteriaList: Criteria[];
  orderByList?: OrderBy[];
  page?: Page;
}
export interface Criteria {
  column: string;
  predicate: string;
  value: string;
  relation?: string;
}
export interface OrderBy {
  column: string;
  order?: string;
}
export interface Page {
  pageSize: number;
  pageIndex: number;
}

export interface ObjectPage {
  records: ObjectInstance[];
  pages: number;
  size: number;
  total: number;
}
export interface ObjectInstance {
  projectCode: string;
  objectCode: string;
  objectName: string;
  objectType: string;
  fieldNum: number;
  methodNum: number;
}

export interface Field {
  projectCode: string;
  objectCode: string;
  fieldCode: string;
  fieldName: string;
  defaultValue: string;
  dataType: string;
  maxLength?: number;
  numericScale?: number;
  dataFieldPos: number;
  relateToOne: string;
  relateToMany: string;
  relateToMany: string;
  idFlag: boolean;
  requireFlag: boolean;
  uniqueFlag: boolean;
  indexFlag: boolean;
  systemFlag: boolean;
  sortNo: number;
}
